use actix_web::{get, web, HttpResponse};

use crate::domain::dto::{Payload};
use crate::services::calc::eval;
use crate::errors::errors::NotSupportedError;
use crate::domain::rules::{Rule};

#[get("/api/v1/calc/{rule}")]
pub async fn base(web::Path(rule): web::Path<String>, item: web::Json<Payload>) -> HttpResponse {
    match eval(item.0, Rule { name: rule }) {
        Ok(result) => HttpResponse::Ok().json(result),
        Err(err) => {
            match err {
                NotSupportedError::RuleNotFound => HttpResponse::NotFound().finish(),
                _ => HttpResponse::BadRequest().finish()
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use actix_web::{test, App};

    #[actix_rt::test]
    async fn should_have_404_when_request_to_incorrect_rule() {
        let mut app = test::init_service(App::new()
            .service(base)).await;
        let data = Payload {
            A: true,
            B: true,
            C: true,
            D: 15.0,
            E: 10,
            F: 20,
        };
        let req = test::TestRequest::get().uri("/api/v1/calc/custom3").set_json(
            &data
        ).to_request();

        let resp = test::call_service(&mut app, req).await;

        assert_eq!(resp.status(), 404);
    }

    #[actix_rt::test]
    async fn should_have_200_when_request_to_custom1_is_OK() {
        let mut app = test::init_service(App::new()
            .service(base)).await;
        let data = Payload {
            A: true,
            B: true,
            C: true,
            D: 15.0,
            E: 10,
            F: 20,
        };
        let req = test::TestRequest::get().uri("/api/v1/calc/custom1").set_json(
            &data
        ).to_request();

        let resp = test::call_service(&mut app, req).await;

        assert!(resp.status().is_success());
    }

    #[actix_rt::test]
    async fn should_have_200_when_request_to_custom2_is_OK() {
        let mut app = test::init_service(App::new()
            .service(base)).await;
        let data = Payload {
            A: true,
            B: true,
            C: true,
            D: 15.0,
            E: 10,
            F: 20,
        };
        let req = test::TestRequest::get().uri("/api/v1/calc/custom2").set_json(
            &data
        ).to_request();

        let resp = test::call_service(&mut app, req).await;

        assert!(resp.status().is_success());
    }

    #[actix_rt::test]
    async fn should_have_200_when_request_to_base_is_OK() {
        let mut app = test::init_service(App::new()
            .service(base)).await;
        let data = Payload {
            A: true,
            B: true,
            C: true,
            D: 15.0,
            E: 10,
            F: 20,
        };
        let req = test::TestRequest::get().uri("/api/v1/calc/base").set_json(
            &data
        ).to_request();

        let resp = test::call_service(&mut app, req).await;

        assert!(resp.status().is_success());
    }

    #[actix_rt::test]
    async fn should_have_40_when_request_to_base_is_incorrect() {
        let mut app = test::init_service(App::new()
            .service(base)).await;
        let data = Payload {
            A: false,
            B: false,
            C: false,
            D: 15.0,
            E: 10,
            F: 20,
        };
        let req = test::TestRequest::get().uri("/api/v1/calc/base").set_json(
            &data
        ).to_request();

        let resp = test::call_service(&mut app, req).await;

        assert_eq!(resp.status().as_u16(), 400);
    }
}
