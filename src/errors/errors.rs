#[derive(Debug, Clone)]
pub enum NotSupportedError {
    RuleError,
    RuleNotFound,
}

