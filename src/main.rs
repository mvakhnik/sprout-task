use actix_web::{
    App, error, HttpResponse, HttpServer, web,
};

mod controllers;
mod domain;
mod services;
mod errors;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    std::env::set_var("RUST_LOG", "actix_web=info");
    env_logger::init();
    HttpServer::new(|| {
        App::new()
            .service(controllers::calc::base)
    })
        //TODO to config
        .bind("127.0.0.1:8080")?
        .run()
        .await
}
