use serde::{Deserialize, Serialize};
use crate::domain::rules::Code;

#[derive(Debug, Serialize, Deserialize)]
pub struct Payload {
    pub A: bool,
    pub B: bool,
    pub C: bool,
    pub D: f32,
    pub E: i32,
    pub F: i32,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Response {
    pub H: Code,
    pub K: f32,
}

