use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize, PartialEq, Clone, Copy)]
pub enum Code {
    M,
    P,
    T,
}

///
/// Rule to define applied logic.
///
///     name - 'base, custom1, custom2'
///
/// There was idea to use enum but it's not very good approach if logic is going to growth
/// string id allows to make it more flexible and use some database.
#[derive(Debug)]
pub struct Rule {
    pub name: String
}
