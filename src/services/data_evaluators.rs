use crate::domain::dto::Payload;
use crate::domain::rules::Code;

struct Base;
struct Custom1;
struct Custom2;
struct ErrorHandler;

pub trait DataEvaluator {
    fn eval(&self, data: &Payload, code: Code) -> Option<f32>;
}

impl DataEvaluator for Base {
    fn eval(&self, data: &Payload, code: Code) -> Option<f32> {
        Some(match code {
            Code::M => data.D + (data.D * data.E as f32 / 10.0),
            Code::P => data.D + (data.D * (data.E - data.F) as f32 / 25.5),
            Code::T => data.D - (data.D * data.F as f32) / 30.0,
            _ => return None
        })
    }
}

impl DataEvaluator for Custom1 {
    fn eval(&self, data: &Payload, code: Code) ->  Option<f32> {
        if code == Code::P {
            return Some(2.0 * data.D + (data.D * data.E as f32 / 100.0));
        }
        None
    }
}

impl DataEvaluator for Custom2 {
    fn eval(&self, data: &Payload, code: Code) ->  Option<f32> {
        if code == Code::M {
            return Some(data.F as f32 + data.D + (data.D * data.E as f32 / 100.0));
        }
        None
    }
}

impl DataEvaluator for ErrorHandler {
    fn eval(&self, data: &Payload, code: Code) ->  Option<f32> {
        None
    }
}

/// Fetch data evaluator based on logic id
pub fn data_evaluator(ruleId: &str) -> Option<Box<dyn DataEvaluator>> {
    return match ruleId {
        "custom2" => Some(Box::new( Custom2{})),
        "custom1" => Some(Box::new( Custom1{})),
        "base" => Some(Box::new( Base{})),
        _ => None
    }
}