use crate::domain::dto::Payload;
use crate::domain::rules::{Rule, Code};

pub trait ConditionEvaluator {
    fn eval(&self, data: &Payload) -> Option<Code>;
}


struct Base;
struct Custom2;
struct EmptyEvaluator;

impl ConditionEvaluator for Base {
    fn eval(&self, data: &Payload) -> Option<Code> {
        return if data.A && data.B && !data.C {
            Some(Code::M)
        } else if data.A && data.B && data.C {
            Some(Code::P)
        } else if !data.A && data.B && data.C {
            Some(Code::T)
        } else {
            None
        };
    }
}

impl ConditionEvaluator for Custom2 {
    fn eval(&self, data: &Payload) -> Option<Code> {
        if data.A && data.B && !data.C {
            return Some(Code::T);
        } else if data.A && !data.B && data.C {
            return Some(Code::M);
        };
        None
    }
}

impl ConditionEvaluator for EmptyEvaluator {
    fn eval(&self, data: &Payload) -> Option<Code> {
        None
    }
}

pub fn condition_evaluator(ruleId: &str) -> Box<ConditionEvaluator> {
    match ruleId {
        "base" => Box::new(Base {}),
        "custom2" => Box::new(Custom2 {}),
        _ => Box::new(EmptyEvaluator)
    }
}
