use crate::domain::dto::{Payload, Response};
use crate::errors::errors::NotSupportedError;
use crate::domain::rules::{Rule, Code};
use crate::services::rules::condition_evaluator;
use crate::services::data_evaluators::data_evaluator;

/// Do main calculations.
pub fn eval(data: Payload, rule: Rule) -> Result<Response, NotSupportedError> {
    let h = calc_condition(&data, &rule).ok_or(NotSupportedError::RuleError)?;
    let k = eval_data(&data, h, &rule)?;
    Ok(Response {
        H: h,
        K: k,
    })
}

fn eval_data(data: &Payload, code: Code, rule: &Rule) -> Result<f32, NotSupportedError> {
    match data_evaluator(&rule.name).ok_or(NotSupportedError::RuleNotFound)?
        .eval(data, code) {
            Some(c) => return Ok(c),
            None => {
                return if rule.name != "base" {
                    eval_data(data, code, &Rule { name: "base".to_string() })
                } else {
                    return Err(NotSupportedError::RuleNotFound);
                };
        }
    };
}

fn calc_condition(data: &Payload, rule: &Rule) -> Option<Code> {
    match condition_evaluator(&rule.name).eval(data) {
        Some(c) => return Some(c),
        None => {
            return if rule.name != "base" {
                calc_condition(data, &Rule { name: "base".to_string() })
            } else {
                None
            };
        }
    };
}


#[cfg(test)]
mod tests {
    use super::*;
    use crate::domain::rules::Code;

    #[test]
    fn should_correctly_eval_when_logic_custom2_and_H_is_P() {
        let rule = Rule { name: "custom2".to_string() };
        let payload = Payload {
            A: true,
            B: true,
            C: true,
            D: 15.0,
            E: 10,
            F: 20,
        };
        let expected_k = payload.D + (payload.D * (payload.E - payload.F) as f32 / 25.5);

        let res = eval(payload, rule).unwrap();

        assert_eq!(Code::P, res.H);
        assert_eq!(expected_k, res.K);
    }

    #[test]
    fn should_correctly_eval_when_custom2_and_H_is_M() {
        let rule = Rule { name: "custom2".to_string() };
        let payload = Payload {
            A: true,
            B: false,
            C: true,
            D: 15.0,
            E: 10,
            F: 20,
        };
        let expected_k =
            payload.F as f32 + payload.D as f32 + (payload.D * payload.E as f32 / 100.0);

        let res = eval(payload, rule).unwrap();

        assert_eq!(Code::M, res.H);
        assert_eq!(expected_k, res.K);
    }

    #[test]
    fn should_correctly_eval_when_custom2_and_H_is_T() {
        let rule = Rule { name: "custom2".to_string() };
        let payload = Payload {
            A: true,
            B: true,
            C: false,
            D: 15.0,
            E: 10,
            F: 20,
        };
        let expected_k = payload.D - (payload.D * payload.F as f32) / 30.0;

        let res = eval(payload, rule).unwrap();

        assert_eq!(Code::T, res.H);
        assert_eq!(expected_k, res.K);
    }

    #[test]
    fn should_correctly_eval_when_logic_custom1_and_H_is_P() {
        let rule = Rule { name: "custom1".to_string() };
        let payload = Payload {
            A: true,
            B: true,
            C: true,
            D: 15.0,
            E: 10,
            F: 20,
        };
        let expected_k = 2.0 * payload.D + (payload.D * payload.E as f32 / 100.0);

        let res = eval(payload, rule).unwrap();

        assert_eq!(Code::P, res.H);
        assert_eq!(expected_k, res.K);
    }

    #[test]
    fn should_correctly_eval_when_logic_custom1_and_H_is_M() {
        let rule = Rule { name: "custom1".to_string() };
        let payload = Payload {
            A: true,
            B: true,
            C: false,
            D: 15.0,
            E: 10,
            F: 20,
        };
        let expected_k = payload.D + (payload.D * payload.E as f32 / 10.0);

        let res = eval(payload, rule).unwrap();

        assert_eq!(Code::M, res.H);
        assert_eq!(expected_k, res.K);
    }

    #[test]
    fn should_correctly_eval_when_logic_custom1_and_H_is_T() {
        let rule = Rule { name: "custom1".to_string() };
        let payload = Payload {
            A: false,
            B: true,
            C: true,
            D: 15.0,
            E: 10,
            F: 20,
        };
        let expected_k = payload.D - (payload.D * payload.F as f32) / 30.0;

        let res = eval(payload, rule).unwrap();

        assert_eq!(Code::T, res.H);
        assert_eq!(expected_k, res.K);
    }

    #[test]
    fn should_correctly_eval_when_base_logic_and_H_is_P() {
        let rule = Rule { name: "base".to_string() };
        let payload = Payload {
            A: true,
            B: true,
            C: true,
            D: 15.0,
            E: 10,
            F: 20,
        };
        let expected_k = payload.D + (payload.D * (payload.E - payload.F) as f32 / 25.5);

        let res = eval(payload, rule).unwrap();

        assert_eq!(Code::P, res.H);
        assert_eq!(expected_k, res.K);
    }

    #[test]
    fn should_correctly_eval_when_base_logic_and_H_is_M() {
        let rule = Rule { name: "base".to_string() };
        let payload = Payload {
            A: true,
            B: true,
            C: false,
            D: 15.0,
            E: 10,
            F: 20,
        };
        let expected_k = payload.D + (payload.D * payload.E as f32 / 10.0);

        let res = eval(payload, rule).unwrap();

        assert_eq!(Code::M, res.H);
        assert_eq!(expected_k, res.K);
    }

    #[test]
    fn should_correctly_eval_when_base_logic_and_H_is_T() {
        let rule = Rule { name: "base".to_string() };
        let payload = Payload {
            A: false,
            B: true,
            C: true,
            D: 15.0,
            E: 10,
            F: 20,
        };
        let expected_k = payload.D - (payload.D * payload.F as f32) / 30.0;

        let res = eval(payload, rule).unwrap();

        assert_eq!(Code::T, res.H);
        assert_eq!(expected_k, res.K);
    }

    #[test]
    fn should_throw_error_when_base_logic_and_condition_not_supported() {
        let rule = Rule { name: "base".to_string() };
        let payload = Payload {
            A: false,
            B: false,
            C: false,
            D: 15.0,
            E: 10,
            F: 20,
        };

        let res = eval(payload, rule);

        assert_eq!(res.is_err(), true);
    }
}