# What is this?

Test project written on [Rust](https://www.rust-lang.org/) and powered by [actix-web](https://github.com/actix/actix-web)
_Rust_ is chosen by obvious reasons.
_actix-web_ choose has been made based on public and local benchmarks as one of the top performed frameworks.

## Task
[Initial task](task/task.pdf)

## Requirements
- Rust language

## Project structure

Project is simple REST service.
Project structure is very straightforward:
- main.rs - enrypoint
  - controllers
  - services
  - ...

Such structure has been chosen to have some close to production view and allow to be improved easier than single file imperative approach.

## API description

REST versioned API:
- GET api/v1/calc/{rule}
    where {rule} is logic to apply (base, custom1, custom2)

Response statuses:
- 200 if ok
- 400 if smth wrong with request
- 404 if url or logic not found

```
// JSON payload
{
    "A": true,
    "B": true,
    "C": true,
    "D": 0.0,
    "E": 1,
    "F": 1
}

// JSON response
{
    "H": "P",
    "K": 0.0
}
```

## Testing

Application is covered (not fully but enough to refactore/improve it safely) with unit tests.
Test it using command below:
```
cargo test
```

### Postman
there is _postman_ folder and Postman json file which can be imported to Postman to verify API

## How to play with app

Start application
```
// from project's root folder
cargo run
```

## How to do some small integration testing

*Base*
```
curl --location --request GET 'localhost:8080/api/v1/calc/base' \
--header 'Content-Type: application/json' \
--data-raw '{
    "A": true,
    "B": true,
    "C": true,
    "D": 2.0,
    "E": 1,
    "F": 1
}'
```

*Base*
```
curl --location --request GET 'localhost:8080/api/v1/calc/base' \
--header 'Content-Type: application/json' \
--data-raw '{
    "A": false,
    "B": false,
    "C": false,
    "D": 2.0,
    "E": 1,
    "F": 1
}'
```

*Custom 1*
```
curl --location --request GET 'localhost:8080/api/v1/calc/custom1' \
--header 'Content-Type: application/json' \
--data-raw '{
    "A": true,
    "B": true,
    "C": true,
    "D": 2.0,
    "E": 1,
    "F": 1
}'
```
*Custom 2*
```
curl --location --request GET 'localhost:8080/api/v1/calc/custom2' \
--header 'Content-Type: application/json' \
--data-raw '{
    "A": true,
    "B": true,
    "C": true,
    "D": 2.0,
    "E": 1,
    "F": 1
}'
```

*Custom 3 (not found)*
```
curl --location --request GET 'localhost:8080/api/v1/calc/custom3' \
--header 'Content-Type: application/json' \
--data-raw '{
    "A": true,
    "B": true,
    "C": true,
    "D": 2.0,
    "E": 1,
    "F": 1
}'
```
